from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model

User = get_user_model()


class Chat(models.Model):
    objects = None
    author = models.ForeignKey(User, on_delete=models.DO_NOTHING, blank=True, null=True)
    message_text = models.TextField()
    published_date = models.DateTimeField(default=timezone.now)


class AddChatUsers(models.Model):
    objects = None
    add_active_users = models.BooleanField(default=False)
