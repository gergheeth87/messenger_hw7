from django.urls import path
from .views import user_login, add_user_perm, chat, connect_to_chat, ChatUpdateView, ChatDeleteView


urlpatterns = [
    path('', user_login, name='user_login'),
    path('add_user_perm/', add_user_perm, name='add_user_perm'),
    path('chat/', chat, name='chat'),
    path('connect_to_chat/', connect_to_chat, name='connect_to_chat'),
    path('<pk>/edit/', ChatUpdateView.as_view(), name='edit'),
    path('<pk>/delete/', ChatDeleteView.as_view(), name='delete'),
]
